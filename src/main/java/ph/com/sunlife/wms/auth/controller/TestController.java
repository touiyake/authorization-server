package ph.com.sunlife.wms.auth.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

	@GetMapping("/integration")
	public ResponseEntity<Void> integration() {
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}