package ph.com.sunlife.wms.auth.dto;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class AccessToken {

	private boolean tokenValid = false;
	private boolean tokenNonExpired = false;
	private Token tokenObject;

}
