package ph.com.sunlife.wms.auth.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter @Getter
@ToString
public class AccessTokenRequest {

	private String username;
	private String password;

}
