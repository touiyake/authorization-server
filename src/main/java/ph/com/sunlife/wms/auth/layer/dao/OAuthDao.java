package ph.com.sunlife.wms.auth.layer.dao;

import java.io.Serializable;

import ph.com.sunlife.wms.auth.abstracts.Operations;

public interface OAuthDao<T extends Serializable> extends Operations<T> {
	
}
