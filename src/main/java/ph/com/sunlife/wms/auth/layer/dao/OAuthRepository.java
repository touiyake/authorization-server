package ph.com.sunlife.wms.auth.layer.dao;

import org.springframework.stereotype.Repository;

import ph.com.sunlife.wms.auth.abstracts.AbstractHibernateDao;
import ph.com.sunlife.wms.auth.entity.OAuthUser;

@Repository
public class OAuthRepository extends AbstractHibernateDao<OAuthUser> implements OAuthDao<OAuthUser> {

	public OAuthRepository() {
		super();
		setClazz(OAuthUser.class);
	}
	
}