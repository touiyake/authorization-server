package ph.com.sunlife.wms.auth.layer.dao;

import org.springframework.stereotype.Repository;

import ph.com.sunlife.wms.auth.abstracts.AbstractHibernateDao;
import ph.com.sunlife.wms.auth.entity.OAuthUser;

@Repository
public class OAuthUserRepository extends AbstractHibernateDao<OAuthUser> implements UserDao<OAuthUser> {

	public OAuthUserRepository() {
		super();
		setClazz(OAuthUser.class);
	}
	
}