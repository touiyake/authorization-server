package ph.com.sunlife.wms.auth.layer.service;

import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.com.sunlife.wms.auth.abstracts.AbstractHibernateService;
import ph.com.sunlife.wms.auth.abstracts.Operations;
import ph.com.sunlife.wms.auth.dto.AccessToken;
import ph.com.sunlife.wms.auth.dto.Token;
import ph.com.sunlife.wms.auth.entity.OAuthUser;
import ph.com.sunlife.wms.auth.layer.dao.OAuthDao;
import ph.com.sunlife.wms.auth.util.Constants;

@Service
@Transactional
public class OAuthCoreService extends AbstractHibernateService<OAuthUser> implements OAuthService<OAuthUser> {

	@Autowired
	private OAuthDao<OAuthUser> dao;
		
	public OAuthCoreService() {
		super();
	}

	@Override
	public OAuthUser generateAccessToken(OAuthUser entity) {
		long currentTime = System.currentTimeMillis() / 1000;
		long expirationTime = currentTime + Constants.TOKEN_EXPIRATION; // 5 minutes token expiration
		
		entity.setTokenExpiration(new Date(expirationTime * 1000));
		entity.setTimestamp(new Date(currentTime * 1000));
		entity.setAccessToken(UUID.randomUUID().toString());
		
		if (this.dao.merge(entity)) {
			return entity;
		}

		return null;
	}
	
	@Override
	public AccessToken initToken(String token) {
		AccessToken accessToken = new AccessToken();
		OAuthUser user = this.dao.findOneByKeyVal("accessToken", token);
		if (user == null) {
			accessToken.setTokenValid(false);
			return accessToken;
		}
		accessToken.setTokenValid(true);
		long currentTime = System.currentTimeMillis() / 1000;
		long expires_in = (user.getTokenExpiration().getTime() / 1000) - currentTime;
		if (expires_in <= 0) {
			accessToken.setTokenNonExpired(false);
			return accessToken;
		}
		accessToken.setTokenNonExpired(true);
		accessToken.setTokenObject(new Token(token, null, expires_in));
		return accessToken;
	}

	@Override
	protected Operations<OAuthUser> getDao() {
		return dao;
	}

}
