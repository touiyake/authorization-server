package ph.com.sunlife.wms.auth.layer.service;

import java.io.Serializable;

import ph.com.sunlife.wms.auth.abstracts.Operations;
import ph.com.sunlife.wms.auth.dto.AccessToken;

public interface OAuthService<T extends Serializable> extends Operations<T> {
	
	public T generateAccessToken(T entity);
	
	public AccessToken initToken(String token);
	
}