package ph.com.sunlife.wms.auth.layer.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.com.sunlife.wms.auth.abstracts.AbstractHibernateService;
import ph.com.sunlife.wms.auth.abstracts.Operations;
import ph.com.sunlife.wms.auth.entity.OAuthUser;
import ph.com.sunlife.wms.auth.layer.dao.UserDao;

@Service
@Transactional
public class OAuthUserService extends AbstractHibernateService<OAuthUser> implements UserService<OAuthUser> {

	@Autowired
	private UserDao<OAuthUser> dao;
	
	public OAuthUserService() {
		super();
	}
	
	@Override
	protected Operations<OAuthUser> getDao() {
		return dao;
	}

}