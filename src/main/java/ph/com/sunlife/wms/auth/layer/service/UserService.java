package ph.com.sunlife.wms.auth.layer.service;

import java.io.Serializable;

import ph.com.sunlife.wms.auth.abstracts.Operations;

public interface UserService<T extends Serializable> extends Operations<T> {

}
